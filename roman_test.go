package roman

import (
	"testing"
)

func TestRegexp(t *testing.T) {
	for i := 1; i < 4_000; i++ {
		s := Format(i)
		if !Regexp.MatchString(s) {
			t.Errorf("%s should be matched", s)
		}
	}
}

func TestFull(t *testing.T) {
	for i := 1; i < 4_000; i++ {
		s := Format(i)
		if !Valid(s) {
			t.Errorf("%s should be valid", s)
		}
		if i != Parse(s) {
			t.Errorf("%d != %s", i, s)
		}
	}
}

func TestFormatZero(t *testing.T) {
	t.Log(Format(0))
}

func TestFormatNegative(t *testing.T) {
	t.Log(Format(-1))
}

func BenchmarkParse(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Parse("MMMDCCCLXXXVIII")
	}
}

func BenchmarkFormat(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Format(3888)
	}
}

func FuzzSafe(f *testing.F) {
	f.Log(len(values))
	f.Fuzz(func(t *testing.T, s string) {
		if Safe(s) {
			_ = Parse(s)
		}
	})
}

func FuzzValid(f *testing.F) {
	f.Log(len(values))
	f.Fuzz(func(t *testing.T, s string) {
		if Valid(s) {
			_ = Parse(s)
		}
	})
}
