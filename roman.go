package roman

import (
	"regexp"
	"strings"
)

var Regexp = regexp.MustCompile(`(?i)M*(?:CM)?D?(?:CD)?C{0,3}(?:XC)?L?(?:XL)?X{0,3}(?:IX)?V?(?:IV)?I{0,3}`)

var values = []int{
	'i': 1, 'I': 1, 'v': 5, 'V': 5,
	'x': 10, 'X': 10, 'l': 50, 'L': 50,
	'c': 100, 'C': 100, 'd': 500, 'D': 500,
	'm': 1000, 'M': 1000,
}

var numerals = []string{
	1: "I", 4: "IV", 5: "V", 9: "IX",
	10: "X", 40: "XL", 50: "L", 90: "XC",
	100: "C", 400: "CD", 500: "D", 900: "CM",
	1000: "M",
}

var denominators = []int{
	1000,
	900, 500, 400, 100,
	90, 50, 40, 10,
	9, 5, 4, 1,
}

// Parse takes a string representing a Roman numeral and returns the number it represents.
// It may panic or return unexpected results if any other string is inputted. Use Safe or Valid to check beforehand.
func Parse(s string) int {
	sum := 0
	l := len(s)
	for i := range []byte(s) {
		j := i + 1
		n := values[s[i]]
		if j != l && values[s[j]] > n {
			sum -= n
		} else {
			sum += n
		}
	}
	return sum
}

// Format takes a number and returns a string representing a Roman numeral.
// It returns an empty string if the number is equal to or less than 0.
func Format(i int) string {
	var raw []byte
	for j, d := range denominators {
		for k := i / d; k > 0; k-- {
			raw = append(raw, numerals[d][0])
			if (j+1)%2 == 0 {
				raw = append(raw, numerals[d][1])
			}
		}
		i %= d
	}
	return string(raw)
}

// Safe takes a string and returns true if it will not cause Parse to panic.
// It doesn't necessarily mean that the string represents a well-formed Roman numeral.
func Safe(s string) bool {
	for _, b := range []byte(s) {
		if int(b) >= len(values) {
			return false
		}
		if values[b] == 0 {
			return false
		}
	}
	return true
}

// Valid takes a string and returns true if it represents a valid and well-formed Roman numeral.
// It checks that it won't panic using Safe, then compares the result of Format(Parse(s)) with the original input.
func Valid(s string) bool {
	return Safe(s) && strings.EqualFold(s, Format(Parse(s)))
}
